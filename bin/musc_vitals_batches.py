#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8:ts=4;sw=4
#
# Copyright © 2018 Evan T. Phelps
#
# Distributed under terms of the MIT license.import getpass
##############################################################################
'''Incremental ETL, MUSC Vital files to CDW.
'''
import collections
import datetime
from getpass import getpass
import os
import re

import pandas as pd

from epana import cryptic
from epana import db


def main():
    # Collect and define parameters
    db_user = get_var('db_user', input)
    db_pass = get_var('db_pass', getpass)
    f_user = get_var('f_user', input)
    f_pass = get_var('f_pass', getpass)
    gpg_pass = get_var('gpg_pass', getpass)

    db_host = 'hssc-cdwr3-dtdb-p.clemson.edu'
    db_sid = 'dtprd2'
    db_conn = db.DbOra(db_user, db_pass, db_host, db_sid)

    f_srvr = 'hssc-cdwr3-hsie-d.clemson.edu'
    f_path = '/datastg/musc/vitals/batch-live'
    f_tmpl = 'MUSC_RDW_VITALS_(\d{8})_(\d{8}).dat.gpg'

    # Get files to process.
    d_last = get_last_loaded_date(db_conn)
    print('Last Date Loaded: ' + str(d_last))
    f_idx_all = get_batch_file_index(f_srvr, f_path, f_user, f_pass, f_tmpl)
    f_to_procs = get_files_to_process(d_last, f_idx_all)

    # Get data from files. Build DataFrame.
    df = None
    for fn in f_to_procs:
        full_path = '%s:%s@%s:%s/%s' % (f_user, f_pass, f_srvr,
                                        f_path, fn)
        print(fn, end='...', flush=True)
        with cryptic.fopen(full_path) as fin:
            s = fin.read()
            dat = cryptic.decrypt(cryptic.BytesIO(s), pwd=gpg_pass,
                                  ostream=True)
            df = pd.read_table(dat, sep='|', dtype=str)
            # this_df = pd.read_table(dat, sep='|', dtype=str)
            # df = this_df if df is None else df.append(this_df)
        # break
    # print(df.head(), flush=True)

    # Prepare for CDW Vital table
        cnames_new = ['musc_unique_id', 'drop1', 'drop2', 'htb_enc_id_ext',
                      'drop3', 'htb_patient_id_ext', 'drop4',
                      'collection_date', 'src_code', 'observation_type',
                      'vital_value_num', 'vital_value_unit', 'drop5', 'drop6',
                      'drop7']
        df.columns = cnames_new
        cnames_keep = [cname for cname in cnames_new
                       if not cname.startswith('drop')]
        df = df[cnames_keep]
        df = df.loc[~(df.vital_value_num.isnull()), :]
        df['vital_value_num'] = df.vital_value_num.astype(float)
        crit_w8s = df.observation_type == 'WEIGHT/SCALE'
        crit_hts = df.observation_type == 'HEIGHT'
        df.loc[crit_w8s, ['vital_value_num']] = \
            df[crit_w8s].vital_value_num / 16 * 0.453592
        df.loc[crit_w8s, ['vital_value_unit']] = 'kg'
        df.loc[crit_w8s, ['observation_type']] = 'WEIGHT'
        df.loc[crit_hts, ['vital_value_num']] = \
            df[crit_hts].vital_value_num * 2.54
        df.loc[crit_hts, ['vital_value_unit']] = 'cm'
        # print(df.head(), flush=True)

        data = list([list(x) for x in df.values])
        for rec in data:
            x = rec[3]
            rec[3] = datetime.datetime(int(x[0:4]),
                                       int(x[4:6]),
                                       int(x[6:8]),
                                       int(x[8:10]),
                                       int(x[10:12])
                                       )
            for ix, x in enumerate(rec):
                if pd.isnull(x):
                    rec[ix] = None

        data = list([tuple(x) for x in data])
        del df

    # Stage data for sql ops
        stg_table = 'hssc_etl.musc_vitals_temp'
        db_conn.execute('delete from %s' % stg_table)
        db_conn.stage_data(stg_table, cnames_keep, data)

    # Merge into vital table
        db_conn.execute('''
            insert into cdw.vital
                ( htb_enc_act_id,
                  htb_enc_act_ver_num,
                  patient_id,
                  mpi_lid,
                  collection_date,
                  src_code,
                  observation_type,
                  vital_value_num,
                  vital_value_unit,
                  institution,
                  datasource_id,
                  sourcesystem
                )
            select
                enc.visit_id htb_enc_act_id,
                enc.htb_enc_act_ver_num,
                enc.patient_id,
                vit.htb_patient_id_ext mpi_lid,
                vit.collection_date,
                vit.src_code,
                vit.observation_type,
                vit.vital_value_num,
                vit.vital_value_unit,
                'MUSC' institution,
                25 datasource_id,
                'EPIC' sourcesystem
            from %s vit
            inner join cdw.visit enc
                on (    vit.htb_enc_id_ext = enc.htb_enc_id_ext
                    and enc.datasource_id = 25 )
            ''' % stg_table)
        print('%d rows inserted.' % db_conn.cursor.rowcount, flush=True)
        db_conn.commit()


def get_var(varname, func_input):
    val = os.getenv(varname)
    if val is None:
        val = func_input('%s: ' % varname)
    return val


def get_last_loaded_date(db_conn):
    db_sql = '''
        select max(collection_date)
        from cdw.vital
        where datasource_id in (1,25)
    '''
    results = list(db_conn.query(db_sql))
    return results[0][0]


def get_d0_d1(f_tmpl, fn):
    res = re.search(f_tmpl, fn)
    if not res:
        return None
    return tuple(int(m) for m in res.groups())


def get_batch_file_index(f_srvr, f_path, f_user, f_pass, f_tmpl):
    file_map = collections.OrderedDict()
    for fn in cryptic.ssh_ls(f_path, srvr=f_srvr, usr=f_user, pwd=f_pass):
        full_path = '%s/%s' % (f_path, fn)
        try:
            (d0, d1) = get_d0_d1(f_tmpl, full_path)
        except Exception as err:
            print(err)
            print('%s does not match expected pattern.' % full_path)
            continue
        file_map[(d0, d1)] = fn
    return file_map


def dt_to_strint(dt):
    yr_str = str(dt.year)
    mo_str = str(dt.month).zfill(2)
    dy_str = str(dt.day).zfill(2)
    dt_str = yr_str + mo_str + dy_str
    return int(dt_str)


def get_files_to_process(last_proc_dt, idx):
    fns = []
    dt = dt_to_strint(last_proc_dt)
    for (d0, d1), fn in idx.items():
        if d1 > dt:
            fns.append(fn)
    return sorted(fns)


if __name__ == '__main__':
    main()

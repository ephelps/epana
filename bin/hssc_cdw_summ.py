#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8:ts=4;sw=4
#
# Copyright © 2018 Evan T. Phelps
#
# Distributed under terms of the MIT license.import getpass
##############################################################################
'''Example of using `cx_oracle` with SqlAlchemy with large arraysize for
better performance than with the default value of 50.  This example does not
attempt to illustrate performance and uses hard-coded server information that
is not generally accessible.
'''
import getpass

from sqlalchemy import create_engine

from epana import tabular

oracle_connection_string = 'oracle+cx_oracle://' + \
    '{username}:{password}@{hostname}:{port}/{database}'


engine = create_engine(
    oracle_connection_string.format(
        username=input('User: '),
        password=getpass.getpass('Password: '),
        hostname='hssc-cdwr3-dtdb-p',
        port='1521',
        database='dtprd2'
    ),
    arraysize=50000
)


def q(sql, engine):
    return tabular.df_from_sql(sql, engine)


if __name__ == '__main__':
    df = q('select * from cdw.visit where rownum < 11', engine=engine)
    tabular.print_full(df)

create table enc_summ_20180511 nologging as
select /*+ parallel 4 */
  enc.datasource_id dsid,
  enc.htb_enc_id_ext local_vnum,
  enc.patient_id patid,
  enc.visit_id encid,
  enc.class_code pat_type,
  enc.type_code enc_type,
  trunc(enc.visit_start_date, 'day') adm_dt,
  trunc(enc.visit_end_date, 'day') disch_dt,
  case when coalesce(dx.n_dxs, 0) + coalesce(px.n_pxs, 0) +
            coalesce(mo.n_mos, 0) + coalesce(lr.n_lrs, 0) +
            coalesce(obs.n_obs, 0) > 0 then 'Y'
    else 'N' end has_cd,
  coalesce(dx.n_pdx, 0) n_pdx,
  coalesce(dx.n_dxs_uniq, 0) n_dxs_uniq,
  coalesce(dx.n_dxs, 0) n_dxs,
  coalesce(px.n_pxs_uniq, 0) n_pxs_uniq,
  coalesce(px.n_pxs, 0) n_pxs,
  coalesce(mo.n_mos_uniq, 0) n_mos_uniq,
  coalesce(mo.n_mos, 0) n_mos,
  coalesce(lr.n_lrs_uniq, 0) n_lrs_uniq,
  coalesce(lr.n_lrs, 0) n_lrs,
  coalesce(obs.n_obs_types, 0) n_obs_types,
  coalesce(obs.n_obs, 0) n_obs
from cdw.visit enc
left outer join (
  select
    visit_id,
    count(case when pdx = 'P' then 1 else NULL end) n_pdx,
    count(distinct dx_code) n_dxs_uniq,
    count(dx_code) n_dxs
  from cdw.diagnosis
  group by visit_id
) dx on ( enc.visit_id = dx.visit_id )
left outer join (
  select
    visit_id,
    count(distinct proc_code) n_pxs_uniq,
    count(proc_code) n_pxs
  from cdw.procedure
  group by visit_id
) px on ( enc.visit_id = px.visit_id )
left outer join (
  select
    visit_id,
    count(distinct med_code) n_mos_uniq,
    count(med_code) n_mos
  from cdw.medication_order
  group by visit_id
) mo on ( enc.visit_id = mo.visit_id )
left outer join (
  select
    visit_id,
    count(distinct loinc_cd) n_lrs_uniq,
    count(loinc_cd) n_lrs
  from cdw.lab_result
  group by visit_id
) lr on ( enc.visit_id = lr.visit_id )
left outer join (
  select
    htb_enc_act_id,
    count(distinct src_code) n_obs_types,
    count(vital_value_num) n_obs
  from cdw.vital
  group by htb_enc_act_id
) obs on ( enc.visit_id = obs.htb_enc_act_id )
;


{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# FHIR Resource to CSV\n",
    "\n",
    "This notebook serves as a *familiarization* exercise and as preparation for the subsequent development of a data integrity testing process for HSSC's under-development Federated FHIR Architecture.  The exercise will ensure that I'm able to access the required servers and perform basic operations that will be involved, more generally, in connecting and comparing inbound source file data with outbound FHIR Resources.  The approach in this exercise will be to implement a reverse mapping from Resource to Source Format.\n",
    "\n",
    "Rather than using the basic `requests` library to construct API URL's, I will use a **Smart-on-FHIR** library, `fhirclient` (see https://github.com/smart-on-fhir/client-py).\n",
    "\n",
    "Tasks:\n",
    "\n",
    "1. Query a FHIR Server to get a Patient Resource based on a local identifier (MRN).\n",
    "1. Convert Resource to source file format.\n",
    "1. Compare the two.\n",
    "\n",
    "MUSC Federated System Information:\n",
    "\n",
    "* FHIR Server:  `hssc-smilecdr-d` (PORT?)\n",
    "* Database:  `CDR_MUSC@CDRDEV` on `hssc-smilecdrdb-d`\n",
    "* Source Files:  `hssc-smilecdr-d:/data/stage/MUSC`\n",
    "\n",
    "***SECURITY NOTE:*** *No data that is accessed in the scope of this notebook should be saved unless it is on an approved server.  The data **may** be viewed within this notebook during a working session but **do not** save the notebook with any data in the output cells.*\n",
    "\n",
    "## Command-line test:\n",
    "\n",
    "Find an MRN (column name = `#PATIENT_ID`) in one of the processed files at `hssc-smilecdr-d:/data/stage/MUSC/mpi/alldone/`.  Use your `<username>:<password>` and `<MRN>` in the following `curl` command to verify that you can connect, are authorized, and get the expected record.\n",
    "\n",
    "```\n",
    "curl --user <username>:<password> -G -v \\\n",
    "     \"http://hssc-smilecdr-d.clemson.edu:38000/Patient\" \\\n",
    "      --data-urlencode \"identifier=urn:hssc:musc:patientid|<MRN>\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get a Patient Resource\n",
    "\n",
    "We will use the MUSC MRN to get a Patient Resource from the MUSC SmileCDR and convert it to a *flat* format for comparison.\n",
    "\n",
    "### Setup SMART Client\n",
    "\n",
    "Currently, I am not using OAuth2 -- only Basic HTTP Authentication.  The FHIR client uses `client_id` and `client_secret` to generate the `base64`-encoded HTTB Authorization header."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "User: ephelps\n",
      "Password: ········\n"
     ]
    }
   ],
   "source": [
    "from getpass import getpass\n",
    "from itertools import dropwhile\n",
    "\n",
    "from fhirclient import client\n",
    "from fhirclient.models import patient\n",
    "from pprint import pprint\n",
    "\n",
    "fhirclient_settings = {\n",
    "    'app_id': 'ep_interactive',\n",
    "    'client_id': input('User: '),\n",
    "    'client_secret': getpass('Password: '),\n",
    "    'api_base': 'http://hssc-smilecdr-d.clemson.edu:38000'\n",
    "}\n",
    "smart = client.FHIRClient(settings=fhirclient_settings)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Let's define some functions\n",
    "\n",
    "Note that these functions are **not** intended to be generally applicable to all FHIR Patient resource variants.  They, instead, are utility functions to facilitate handling a specific set of Patient resources derived from MUSC incremental data extracts.  Accordingly, they exploit specifics of that set of Patient resources."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def get_patient_by_MRN(mrn, sys_uri='urn:hssc:musc:patientid', server=smart.server):\n",
    "    if mrn is None or mrn == '':\n",
    "        raise ValueError('Argument mrn must be a non-empty string!')\n",
    "    val_identifier = '%s|%s'%(sys_uri, mrn)\n",
    "    search = patient.Patient.where(struct={'identifier': val_identifier})\n",
    "    pats = search.perform_resources(smart.server)\n",
    "    n_pats = len(pats)\n",
    "    if n_pats != 1:\n",
    "        raise ValueError('Expected ONE Patient but found %d'%n_pats +\n",
    "                         ' (identifier = %s)'%val_identifier)\n",
    "    return pats[0]\n",
    "\n",
    "\n",
    "def get_id_from_patient(pat, idsys):\n",
    "    ids = [idthis.value for idthis in pat.identifier\n",
    "            if idthis.system == idsys]\n",
    "    n_ids = len(ids)\n",
    "    if n_ids != 1:\n",
    "        raise ValueError('Expected 1 identifer for Patient but found %d'%n_ids +\n",
    "                         ' (identifier = %s)'%pat.id)\n",
    "    return ids[0]\n",
    "\n",
    "\n",
    "def get_MRN_from_patient(pat, mrnsys='urn:hssc:musc:patientid'):\n",
    "    return get_id_from_patient(pat, mrnsys)\n",
    "\n",
    "\n",
    "def get_SSN_from_patient(pat):\n",
    "    ssnsys = 'http://hl7.org/fhir/sid/us-ssn'\n",
    "    ssn = None\n",
    "    try:\n",
    "        ssn = get_id_from_patient(pat, ssnsys)\n",
    "    except ValueError as ve:\n",
    "        pass\n",
    "    return ssn\n",
    "\n",
    "\n",
    "def get_extension_code(pat, sys):\n",
    "    '''This is only good for extension that have a valueCoding element.  If\n",
    "    sys is a list, then it is assumed to specify a hierarchy of extensions by\n",
    "    sys uri.  If sys is a string, then each slash-delimited token beyond the\n",
    "    token 'StructureDefinition' is assumed to indicate another sub-extension.\n",
    "    '''\n",
    "    res = pat\n",
    "    if isinstance(sys,str):\n",
    "        sys_tokens = sys.split('/')\n",
    "        for i, (iel,el) in enumerate(dropwhile(lambda x: x[1] != 'StructureDefinition',\n",
    "                                               enumerate(sys_tokens))):\n",
    "            if i > 0:\n",
    "                if res.extension is None:\n",
    "                    return ''\n",
    "                exts = [ext for ext in res.extension if ext.url == '/'.join(sys_tokens[:iel+1])]\n",
    "                n_exts = len(exts)\n",
    "                if n_exts > 1:\n",
    "                    raise ValueError('More than one extension found for ' +\n",
    "                                     ' patient = %s, url = %s)'%(pat.id, sys))\n",
    "                if n_exts == 0:\n",
    "                    return ''\n",
    "                res = exts[0]\n",
    "    elif isinstance(sys,list):\n",
    "        for system in sys:\n",
    "            if res.extension is None:\n",
    "                return ''\n",
    "            exts = [ext for ext in res.extension if ext.url == system]\n",
    "            n_exts = len(exts)\n",
    "            if n_exts > 1:\n",
    "                raise ValueError('More than one extension found for ' +\n",
    "                                 ' patient = %s, url = %s)'%(pat.id, sys))\n",
    "            if n_exts == 0:\n",
    "                return ''\n",
    "            res = exts[0]\n",
    "    \n",
    "    if res is None:\n",
    "        return None\n",
    "    return res.valueCoding.code  # optimistically assume it exists\n",
    "\n",
    "\n",
    "def get_telecom(pat, sys, use):\n",
    "    if pat.telecom:\n",
    "        for tel in pat.telecom:\n",
    "            if tel.system == sys and tel.use == use:\n",
    "                return tel.value\n",
    "    return ''\n",
    "\n",
    "\n",
    "def translate_elements(lst, frm_el=None, to_el=''):\n",
    "    if frm_el:\n",
    "        return [el if el == frm_el else to_el for el in lst]\n",
    "    return [el if el else to_el for el in lst]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Let's take a look at a Resource as JSON"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "mrn = input('MRN: ')  # 005723838\n",
    "pat = get_patient_by_MRN(mrn)\n",
    "pprint(pat.as_json())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get some Source Patient Records"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from edatls import cryptic\n",
    "from edatls import tabular\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "User: phelpse\n",
      "Password: ········\n"
     ]
    }
   ],
   "source": [
    "filesrvr_settings = {\n",
    "    'srvr': 'hssc-smilecdr-d',\n",
    "    'usr': input('User: '),\n",
    "    'pwd': getpass('Password: ')\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['PH', 'MUSC']"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "list(cryptic.ssh_ls('/data/stage/', **filesrvr_settings))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "password (phelpse@hssc-smilecdr-d): ········\n",
      "CPU times: user 5.09 s, sys: 605 ms, total: 5.69 s\n",
      "Wall time: 48.2 s\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "\n",
    "fns = list(cryptic.ssh_ls('/data/stage/MUSC/mpi/alldone/', **filesrvr_settings))\n",
    "\n",
    "fns = [fns[i] for i in range(51)]\n",
    "n_samples_each = [20]*len(fns)\n",
    "\n",
    "df = None\n",
    "for fn, n_samples in zip(fns, n_samples_each):\n",
    "    url = 'phelpse@hssc-smilecdr-d:/data/stage/MUSC/mpi/alldone/'+fn\n",
    "    with cryptic.fopen(url) as fin:\n",
    "        # instead of sampling, I'll just take the first records from each file\n",
    "        this_df = pd.read_table(fin, sep='|', dtype=str, nrows=n_samples)\n",
    "        this_df['fname'] = fn\n",
    "        df = this_df if df is None else df.append(this_df, ignore_index=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# assume records are chronologically ordered and keep last of each patient_id\n",
    "df = df.groupby('#PATIENT_ID').tail(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transform the Resource\n",
    "\n",
    "### Define an MUSC Resource Mapping\n",
    "\n",
    "The following mapping is analogous to a special-case reversal of `handlePatient` in `musc.js` (https://goo.gl/Zj2jxx)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "url_religion = 'http://hl7.org/fhir/StructureDefinition/patient-religion'\n",
    "url_race = 'http://hl7.org/fhir/us/core/StructureDefinition/us-core-race'\n",
    "url_ethnicity = ['http://hl7.org/fhir/us/core/StructureDefinition/us-core-ethnicity', 'ombCategory']\n",
    "url_military_status = 'http://healthsciencessc.org/fhir/StructureDefinition/patient-military-information/military-status'\n",
    "url_nationality = 'http://hl7.org/fhir/StructureDefinition/patient-nationality'\n",
    "\n",
    "map_gender = {'male':'M', 'female':'F', 'unknown':'?'}\n",
    "\n",
    "map_race = {\n",
    "    '2054-5': 'B',\n",
    "    '1002-5': 'I',\n",
    "    '2135-2': 'L',\n",
    "    '2131-1': 'M',\n",
    "    '2028-9': 'O',\n",
    "    '2076-8': 'P',\n",
    "    '2131-1': 'U',\n",
    "    '2106-3': 'W'\n",
    "}\n",
    "\n",
    "map_marital_status = {\n",
    "    'D': 'D',\n",
    "    'M': 'M',\n",
    "    'T': 'P',\n",
    "    'S': 'S',\n",
    "    'UNK': 'U',\n",
    "    'W': 'W',\n",
    "    'L': 'X'\n",
    "}\n",
    "\n",
    "map_country = {\n",
    "    'USA': '1'\n",
    "}\n",
    "\n",
    "map_language = {\n",
    "    'zh': 'CHI',\n",
    "    'en': 'ENG',\n",
    "    'fr': 'FRE',\n",
    "    'de': 'GER',\n",
    "    'it': 'ITAL',\n",
    "    'ja': 'JAP',\n",
    "    'UNK': 'OTH',\n",
    "    'pt': 'PORT',\n",
    "    'ru': 'RUSS',\n",
    "    'ase': 'SIGN',\n",
    "    'es': 'SPA',\n",
    "    'vi': 'VIET'\n",
    "}\n",
    "\n",
    "map_hispanic = {\n",
    "    '2135-2': '1',\n",
    "    '2186-5': '0'\n",
    "}\n",
    "\n",
    "output_map = {\n",
    "    '#PATIENT_ID': get_MRN_from_patient,\n",
    "     'SSN': get_SSN_from_patient,\n",
    "     'PAT_LAST_NAME': lambda pat: pat.name[0].family if pat.name else '',\n",
    "     'PAT_FIRST_NAME': lambda pat: pat.name[0].given[0] if pat.name else '',\n",
    "     'PAT_MIDDLE_NAME': lambda pat: pat.name[0].given[1] if pat.name and len(pat.name[0].given) > 1 else '',\n",
    "     'PAT_NAME_SUFFIX': lambda pat: pat.name[0].suffix if pat.name else '',\n",
    "     'TITLE': lambda pat: pat.name[0].prefix if pat.name else '',\n",
    "     'BIRTH_DATE': lambda pat: pat.birthDate.isostring.replace('-','')+'000000' if pat.birthDate else '',\n",
    "     'GENDER': lambda pat: map_gender.get(pat.gender, pat.gender),\n",
    "     'RACE':  lambda pat: map_race.get(get_extension_code(pat, url_race), get_extension_code(pat, url_race)),\n",
    "     'HISPANIC': lambda pat: map_hispanic.get(get_extension_code(pat, url_ethnicity), get_extension_code(pat, url_ethnicity)),\n",
    "     'MARITAL_STATUS': lambda pat: map_marital_status.get(pat.maritalStatus.coding.code, pat.maritalStatus.coding.code) if pat.maritalStatus else '',\n",
    "     'LANGUAGE': lambda pat: map_language.get(pat.communication.language.coding.code, pat.communication.language.coding.code) if pat.communication else '',\n",
    "     'RELIGION': lambda pat: get_extension_code(pat, url_religion),\n",
    "     'MILITARY_STATUS': lambda pat: get_extension_code(pat, url_military_status),\n",
    "     'ADD_LINE_1': lambda pat: pat.address[0].line[0] if pat.address and pat.address[0].line else '',\n",
    "     'ADD_LINE_2': lambda pat: '' if pat.address is None or pat.address[0].line is None or len(pat.address[0].line) < 2 else pat.address[0].line[1],\n",
    "     'ADD_LINE_3': lambda pat: '' if pat.address is None or pat.address[0].line is None or len(pat.address[0].line) < 3 else pat.address[0].line[2],\n",
    "     'CITY': lambda pat: pat.address[0].city if pat.address else '',\n",
    "     'STATE': lambda pat: pat.address[0].state if pat.address else '',\n",
    "     'ZIP': lambda pat: pat.address[0].postalCode if pat.address else '',\n",
    "     'COUNTY': lambda pat: pat.address[0].district if pat.address else '',\n",
    "     'COUNTRY': lambda pat: map_country.get(pat.address[0].country, pat.address[0].country) if pat.address else '',\n",
    "     'Nationality': lambda pat: get_extension_code(pat, url_nationality),\n",
    "     'DEATH_DATE': lambda pat: pat.deceasedDateTime.isostring.replace('-','') if pat.deceasedDateTime else '',\n",
    "     'EMAIL_ADDRESS': lambda pat: get_telecom(pat, 'email', 'home'),\n",
    "     'HOME_PHONE': lambda pat: get_telecom(pat, 'phone', 'home'),\n",
    "     'MOBILE_PHONE': lambda pat: get_telecom(pat, 'phone', 'mobile'),\n",
    "     'WORK_PHONE': lambda pat: get_telecom(pat, 'phone', 'work')\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Look at the resulting flattened format"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "pprint(translate_elements([v(pat) for k,v in output_map.items()]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Put it together"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 13.4 s, sys: 208 ms, total: 13.6 s\n",
      "Wall time: 5min 15s\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "\n",
    "def get_patient(mrn):\n",
    "    try:\n",
    "        pat = get_patient_by_MRN(mrn)\n",
    "    except ValueError as ve:\n",
    "        return None\n",
    "    return pat\n",
    "\n",
    "\n",
    "def get_resource_as_dict(pat, cols, funcmap=output_map):\n",
    "    if pat is None:\n",
    "        return {k:'' for k in cols}\n",
    "    return {k:funcmap.get(k, lambda x: '')(pat) for k in cols}\n",
    "\n",
    "\n",
    "flat_resources = [ get_resource_as_dict(get_patient(mrn), df.columns)\n",
    "                   for mrn in df['#PATIENT_ID'] ]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Fill nulls and align on indexes of source-data df\n",
    "df.fillna('', inplace=True)\n",
    "df_smile = ( pd.DataFrame(flat_resources)[df.columns[:-1]]\n",
    "             .fillna('')\n",
    "             .set_index(df.index) )\n",
    "\n",
    "# ... but set nulls on all fields of records that don't exist in SmileCDR\n",
    "crit_missing = df_smile['#PATIENT_ID']==''\n",
    "df_smile.loc[crit_missing,:] = pd.np.NaN"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "913 913\n",
      "Index(['#PATIENT_ID', 'PatientIDAssigningAuthority', 'AlternateIdentifiers',\n",
      "       'SSN', 'PAT_LAST_NAME', 'PAT_FIRST_NAME', 'PAT_MIDDLE_NAME',\n",
      "       'PAT_NAME_SUFFIX', 'TITLE', 'BIRTH_DATE', 'GENDER', 'RACE', 'HISPANIC',\n",
      "       'MARITAL_STATUS', 'LANGUAGE', 'RELIGION', 'MILITARY_STATUS',\n",
      "       'ADD_LINE_1', 'ADD_LINE_2', 'ADD_LINE_3', 'CITY', 'STATE', 'ZIP',\n",
      "       'COUNTY', 'COUNTRY', 'Citizenship', 'Nationality', 'DEATH_DATE',\n",
      "       'PATIENT_STATUS', 'CauseOfDeath', 'EMAIL_ADDRESS', 'HOME_PHONE',\n",
      "       'MOBILE_PHONE', 'WORK_PHONE', 'fname'],\n",
      "      dtype='object')\n",
      "Index(['#PATIENT_ID', 'PatientIDAssigningAuthority', 'AlternateIdentifiers',\n",
      "       'SSN', 'PAT_LAST_NAME', 'PAT_FIRST_NAME', 'PAT_MIDDLE_NAME',\n",
      "       'PAT_NAME_SUFFIX', 'TITLE', 'BIRTH_DATE', 'GENDER', 'RACE', 'HISPANIC',\n",
      "       'MARITAL_STATUS', 'LANGUAGE', 'RELIGION', 'MILITARY_STATUS',\n",
      "       'ADD_LINE_1', 'ADD_LINE_2', 'ADD_LINE_3', 'CITY', 'STATE', 'ZIP',\n",
      "       'COUNTY', 'COUNTRY', 'Citizenship', 'Nationality', 'DEATH_DATE',\n",
      "       'PATIENT_STATUS', 'CauseOfDeath', 'EMAIL_ADDRESS', 'HOME_PHONE',\n",
      "       'MOBILE_PHONE', 'WORK_PHONE'],\n",
      "      dtype='object')\n",
      "(913, 35) (913, 34)\n",
      "1019 1019\n"
     ]
    }
   ],
   "source": [
    "print(len(df), len(df_smile))\n",
    "print(df.columns)\n",
    "print(df_smile.columns)\n",
    "print(df.shape, df_smile.shape)\n",
    "print(max(df.index), max(df_smile.index))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "diff_df = (df.iloc[:,:-1] == df_smile).applymap(lambda x: 1 if x else 0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "diff_df = (df.iloc[:,[:-1]] == df_smile).applymap(lambda x: 1 if x else 0)\n",
    "diff_df['fname'] = df['fname'].str[-16:]\n",
    "diff_df.groupby('fname').apply(lambda x: x.sum()/len(x))[diff_df.columns[:-1]].T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "df['f_match'] = diff_df[diff_df.columns[:-1]].apply(sum, axis=1)/len(diff_df.columns[:-1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "df_rowsumm_per_file = pd.DataFrame(df.groupby('fname').f_match.value_counts())\n",
    "df_rowsumm_per_file.columns = ['nrecs']\n",
    "df_rowsumm_per_file = df_rowsumm_per_file.reset_index()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "df_rowsumm_per_file.groupby(df_rowsumm_per_file.f_match.round(2)).nrecs.sum().sort_index()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "diff_df = (df[df.columns[:-1]] == df_smile).applymap(lambda x: 1 if x else 0)\n",
    "diff_df['fname'] = df['fname'].str[-16:]\n",
    "# diff_df.groupby('fname').apply(lambda x: x.sum()/len(x))[diff_df.columns[:-1]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "diff_df[diff_df.columns[:-1]].apply(lambda x: x.sum()/len(x)).sort_values(ascending=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "tabular.get_summary(df).loc[df.columns[0:-2],:]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "tabular.get_summary(df_smile).loc[df_smile.columns[0:-1],:]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "df_both = df.merge(df_smile, left_index=True, right_index=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "df_both = df_both.reindex(sorted(df_both.columns), axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "df['origin'] = 'source'\n",
    "df_smile['origin'] = 'smile'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "df.head().T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": "block",
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

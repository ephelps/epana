select
    drug_type_concept_id,
    cncpt2.concept_name,
    cncpt.concept_class_id,
    count(1) n_recs,
    round(100*count(1)/sum(count(1))
            over (partition by drug_type_concept_id),
          4) p_recs_by_type,
    round(100*count(1)/sum(count(1)) over (),
          4) p_recs_of_all
from drug_exposure de
left outer join concept cncpt
    on (    de.drug_concept_id = cncpt.concept_id
        and cncpt.domain_id = 'Drug'
        and cncpt.vocabulary_id like 'RxNorm%' )
left outer join concept cncpt2
    on (    de.drug_type_concept_id = cncpt2.concept_id
        and cncpt2.concept_class_id = 'Drug Type' )
group by
    cncpt2.concept_name,
    drug_type_concept_id,
    cncpt.concept_class_id
order by
    drug_type_concept_id,
    n_recs desc
;

select trunc(drug_exposure_start_date, 'month') dt, count(1)
from drug_exposure
group by trunc(drug_exposure_start_date, 'month')
order by dt desc
;

select drug_concept_id, drug_source_value
from drug_exposure
--where drug_concept_id = 0
;

select
    drug_type_concept_id,
    length(substr(drug_source_value, instr(drug_source_value, '|')+1)) cd_len,
    count(1) n_recs
from drug_exposure
group by drug_type_concept_id, length(substr(drug_source_value, instr(drug_source_value, '|')+1))
order by cd_len desc
;


select
    drug_type_concept_id,
    length(substr(drug_source_value, instr(drug_source_value, '|')+1)) cd_len,
    count(1) n_recs
from drug_exposure
group by drug_type_concept_id, length(substr(drug_source_value, instr(drug_source_value, '|')+1))
order by cd_len desc
;

select *
from drug_exposure
where drug_source_value like 'urn:hssc:musc%'
;

select *
from concept
--where vocabulary_id = 'RxNorm'
where concept_code = '630208'
;

select *
from drug_exposure
where drug_source_value = 'urn:hssc:musc:medicationcode|54569-3899-0'
;

select * from drug_exposure
where concept_id = 0
;
